import {EMAIL_CHANGED, LOGIN_USER_FAIL, LOGIN_USER_SUCCESS, PASSWORD_CHANGED,LOGIN_USER} from "../types";
import {Alert, AsyncStorage} from 'react-native';
import Toast from 'react-native-simple-toast';
import React from 'react';



export const emailChanged = (text) => {
    return {
        type: EMAIL_CHANGED,
        payload: text
    };
};

export  const passwordChanged =(text) =>{
    return {
        type:PASSWORD_CHANGED,
        payload: text
    };

};

export const logOut=()=>{
    AsyncStorage.setItem('@name', '');
    AsyncStorage.setItem('@id', '');
    AsyncStorage.setItem('@address', '');
    AsyncStorage.setItem('@mobile', '');
    AsyncStorage.setItem('@email', '');
}


export const loginUser=({email,password}) =>{
    return (dispatch) => {
        dispatch({ type: LOGIN_USER });
        var user={
            email:email,
            password:password
        }
        if(user.email === '' || user.password === '') {
            Alert.alert('Enter details to signin!')
          } else {
            fetch('http://fawaskallayi-001-site1.etempurl.com/Register/CustomerLogin', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                Email: email,
                Password: password
              })
            }).then((response) => response.json())
                .then((responseJson) => {
                  var data=JSON.parse(responseJson);
                  if(data.UserId==null){
                    Toast.showWithGravity('Login Failed.',Toast.LONG,Toast.BOTTOM);
                    loginUserFail(dispatch, user)
                    //loginUserFail(dispatch, error);
                  }
                  else{
                    const name=data.FirstName+" "+data.LastName;
                    AsyncStorage.setItem('@name', name);
                    AsyncStorage.setItem('@id', data.UserId);
                    AsyncStorage.setItem('@address', data.Address);
                    AsyncStorage.setItem('@mobile', data.Mobile);
                    AsyncStorage.setItem('@email', data.Email);
                    loginUserSuccess(dispatch, user);
                  }
                })
                .catch((error) => {
                  alert(error.message);
                  loginUserFail(dispatch,error)
                  //this.props.navigation.navigate('Login');
                });
          }
        //firebase.auth().signInWithEmailAndPassword(email, password)
            //.then(user => loginUserSuccess(dispatch, user))
            //.catch((error) => {
                // console.log(error);

                // firebase.auth().createUserWithEmailAndPassword(email, password)
                //     .then(user => loginUserSuccess(dispatch, user))
                //     .catch(() => loginUserFail(dispatch));
                //loginUserFail(dispatch,error)
            //});
    };
};

const loginUserFail=(dispatch,error)=>{
    console.log(error);
    dispatch({type:LOGIN_USER_FAIL});
};

const loginUserSuccess=(dispatch,user)=>{
    dispatch({
        type:LOGIN_USER_SUCCESS,
        payload:user
    });

}