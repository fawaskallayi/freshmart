import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {ScrollView,SafeAreaView} from 'react-native';
import {Drawer} from 'react-native-paper';
import WelcomeScreen from '../screens/WelcomeScreen';
import LoginScreen from '../screens/LoginScreen';
import SignupScreen from '../screens/SignupScreen';
import HomeScreen from '../screens/HomeScreen';
import RestaurantScreen from '../screens/RestaurantScreen';
import MenuScreen from '../screens/MenuScreen';
import OrderScreen from '../screens/OrderScreen';
import CartScreen from '../screens/CartScreen';
import DetailPage from '../screens/OrderDetails';
import OrderTracking from '../screens/OrderTrack';
import {logOut} from '../actions';
import { Text, Appbar, Title, Avatar, Card, TextInput } from "react-native-paper";

const DrawerNavigator = createDrawerNavigator(
    {
        Home:{screen:HomeScreen},
        Restaurants:{screen:RestaurantScreen},
        Menus:{screen:MenuScreen},
        Orders:{screen:OrderScreen},
        Cart:{screen:CartScreen},
        OrderDetail:{screen:DetailPage},
        TrackOrder:{screen: OrderTracking}
    },
    {
        contentComponent: props => (
            <ScrollView>
              <SafeAreaView forceInset={{ top: "always", horizontal: "never" }}>
                <Drawer.Item
                  label="Home"
                  active="true"
                  onPress={() => props.navigation.navigate("Home")}
                />
                <Drawer.Item
                    label="Orders"
                    onPress={() => props.navigation.navigate("Orders")}
                />
                <Drawer.Item
                    label="Cart"
                    onPress={()=> props.navigation.navigate("Cart")}
                />
                <Drawer.Item
                    label="Logout"
                    onPress={()=> { logOut(); props.navigation.navigate("Login"); }}
                />
              </SafeAreaView>
            </ScrollView>
          )
    }
)


navigationOptions = ({ navigation }) => ({
    header: null
})

const AppNavigator= createStackNavigator({
    Welcome:{screen:WelcomeScreen},
    Login:{screen:LoginScreen},
    Signup:{screen:SignupScreen},
    Drawer:{screen:DrawerNavigator,navigationOptions}
},
);

export default createAppContainer(AppNavigator);