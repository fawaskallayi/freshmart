//import liraries
import React, { Component } from "react";
import { View,StyleSheet,ScrollView,FlatList,SafeAreaView,AsyncStorage,Picker,Alert,BackHandler } from "react-native";
import { Card,Appbar,Button,Text,TextInput} from "react-native-paper";
import {DrawerActions} from "react-navigation-drawer";
import Toast from 'react-native-simple-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import {NavigationEvents} from "react-navigation";
// create a component
class CartScreen extends Component {
  constructor(props){
    super(props);
    this.state={
        userId:0,
        cartData:null,
        total:0.00,
        pincode:'',
        validpin:false,
        delCharge:0.00,
        minOrder:0.00,
        postid:null,
        totaltopay:0.00,
        delAddress:'',
        paymentType:'',
        spinner:false,
        key:0,
    }
  }

  componentDidMount(){
    this.loadData();
  }

  async loadData(){
    await AsyncStorage.getItem('@id').then(value =>
      //AsyncStorage returns a promise so adding a callback to get the value
      this.setState({ userId: value })
      //Setting the value in Text
    );
    await AsyncStorage.getItem('address').then(value =>
      //AsyncStorage returns a promise so adding a callback to get the value
      this.setState({ delAddress: value })
      //Setting the value in Text
    );
    this.setState({delAddress:this.state.delAddress.replace(/"/g,"")})
    this.renderCartItems();
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
}

placeOrder(){
  this.setState({spinner:true});
  fetch('http://fawaskallayi-001-site1.etempurl.com/Order/PlaceOrder', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      CustomerId:parseInt(this.state.userId),
      Amount:this.state.total,
      Current:new Date(),
      PaymentMethod:this.state.paymentType,
      DeliveryAddress:this.state.delAddress,
      DeliveryCharge:this.state.delCharge,
      TotalAmount:this.state.totaltopay,
      PostalCodeId:this.state.pinid,
      CurrentStatus:"Ordered",
      Items:this.state.cartData,
    })
  }).then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner:false});
        var data=JSON.parse(responseJson);
        if(data){
          Toast.showWithGravity("Order Recieved",Toast.LONG,Toast.BOTTOM);
          this.setState({cartData:null});
          this.loadData();
          this.props.navigation.navigate("Orders");
        }
      })
      .catch((error) => {
        this.setState({spinner:false});
        alert(error.message);
      });
}

orderSubmit(){
  if(this.state.delAddress===""){
    Toast.showWithGravity("Enter your Delivery Address",Toast.LONG,Toast.BOTTOM);
    return;
  }
  if(this.state.paymentType===""){
    Toast.showWithGravity("Select Payment Type",Toast.LONG,Toast.BOTTOM);
    return;
  }
  if(this.state.paymentType=="CashonDelivery"){
      this.placeOrder();
  }
}

deleteCartItem(id){
  this.setState({spinner:true});
  fetch('http://fawaskallayi-001-site1.etempurl.com/Cart/RemoveCartItem', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      Id:id
    })
  }).then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner:true});
        var data=JSON.parse(responseJson);
        if(data){
          Toast.showWithGravity("Removed from Cart",Toast.LONG,Toast.BOTTOM);
          this.renderCartItems();
        }
      })
      .catch((error) => {
        this.setState({spinner:true});
        alert(error.message);
      });
}

 validatePincode(){
   var pinCode=this.state.pincode;
   if(pinCode===""){
      this.setState({validpin:false});
      Toast.showWithGravity('Enter Pincode',Toast.LONG,Toast.BOTTOM);
      return;
   }
   if(pinCode.length != 6){
    this.setState({validpin:false});
    Toast.showWithGravity('Invalid Pincode '+pinCode,Toast.LONG,Toast.BOTTOM);
    return;
   }
   else{
    this.setState({spinner:true});
    fetch('http://fawaskallayi-001-site1.etempurl.com/Cart/ValidatePincode', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Pincode:pinCode
      })
    }).then((response) => response.json())
        .then((responseJson) => {
          this.setState({spinner:false});
          var data=JSON.parse(responseJson);
          if(data.Id>0){
            this.setState({validpin:true});
            this.setState({pinid:data.Id});
            this.setState({minOrder:data.Minimum});
            if(this.state.total<data.Minimum){
              this.setState({delCharge:data.DeliveryCharge});
            }
            this.setState({totaltopay:this.state.total+this.state.delCharge});
          }
          else{
            Toast.showWithGravity("Currently Unavailable to "+this.state.pincode,Toast.LONG,Toast.BOTTOM);
          }
        })
        .catch((error) => {
          this.setState({spinner:false});
          alert(error.message);
        });
   }
 }

  renderCartItems = () => {
    if(this.state.userId!=0){
      this.setState({spinner:true});
    fetch('http://fawaskallayi-001-site1.etempurl.com/Cart/GetAllCart', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          UserId:parseInt(this.state.userId)
        })
      }).then((response) => response.json())
          .then((responseJson) => {
            this.setState({total:0});
            this.setState({validpin:false});
            this.setState({spinner:false});
            var data=JSON.parse(responseJson);
            if(data.length>0){
            this.setState({cartData:data})
            this.state.cartData.forEach(element => {
              this.setState({total:this.state.total + element.Cost});
            });
            }
          })
          .catch((error) => {
            this.setState({spinner:false});
            alert(error.message);
          });
        }
  }

  renderItems = () => {
    var obj=[];var i=0;
    if(this.state.cartData!=null){
      this.state.cartData.forEach(element => {
        i++;
        obj.push(
          <>
          <Card>
            <Card.Content style={styles.cardContainer}>
              <Button>{i}</Button>
              <Text>{element.Qty} {element.UnitShort}</Text>
              <Text>{element.ProductName}</Text>
              <Text>₹{element.Cost}</Text>
              <Appbar.Action onPress={()=>this.deleteCartItem(element.Id)} icon="delete" />
            </Card.Content>
          </Card>
          </>
        );
      });
      obj.push(
        <View style={styles.totalContainer}>
        <Text>Total</Text>
        <Text>₹{this.state.total}</Text>
      </View>
      );
    }
    return obj;
  }

  renderButtons = () => {
    var obj=[];
    if(this.state.cartData != null){
      if(this.state.validpin){
      obj.push(
        <>
        <View style={{paddingLeft:10,paddingRight:10}}>
        <Text>Check Availability</Text>
        <TextInput
                    style={styles.spaceevenly}
                    mode="outlined"
                    label="Enter your pincode"
                    keyboardType="phone-pad"
                    value={this.state.pincode}
                    onChangeText={(val) => this.updateInputVal(val, 'pincode')}
        />
        <Button style={styles.orderButton}
        onPress={()=>this.validatePincode()}>Check</Button>
        <Text>Note: Delivery is free for a minimum order of ₹{this.state.minOrder}.</Text>
        <TextInput
                    style={styles.spaceevenly}
                    mode="outlined"
                    label="Full Delivery Address"
                    keyboardType="default"
                    value={this.state.delAddress}
                    onChangeText={(val) => this.updateInputVal(val, 'delAddress')}
        />
            <TextInput
              style={styles.spaceevenly}
              mode="outlined"
              keyboardType="default"
              render={(props) => (
              <Picker
        selectedValue={this.state.paymentType}
        style={ styles.spaceevenly}
        onValueChange={(itemValue, itemIndex) => this.updateInputVal(itemValue, 'paymentType')}
      >
        <Picker.Item label="Select Payment Type" value="" />
        <Picker.Item label="Cash on Delivery" value="CashonDelivery" />
      </Picker>
               )} />
        <Button mode="contained">
            Delivery Charge ₹{this.state.delCharge}</Button>
            <Button mode="contained">
            Total to Pay ₹{this.state.totaltopay}</Button>
        <Button style={styles.orderButton}
        onPress={()=>this.orderSubmit()}>Place Order</Button>
        </View>
        </>
      );
      }
      else{
        obj.push(
        <>
        <View style={{paddingLeft:10,paddingRight:10}}>
        <Text>Check Availability</Text>
        <TextInput
                    style={styles.spaceevenly}
                    mode="outlined"
                    label="Enter your pincode"
                    keyboardType="phone-pad"
                    value={this.state.pincode}
                    onChangeText={(val) => this.updateInputVal(val, 'pincode')}
        />
        <Button style={styles.orderButton}
        onPress={()=>this.validatePincode()}>Check</Button>
        </View>
        </>
        );
      }
    }
    else{
      obj.push(
        <>
        <Text style={{fontWeight:'bold',textAlign:'center',paddingTop:20}}>No Items in Cart</Text>
        </>
      );
    }
    return obj;
  }

  render() {
    return (
      <View>
        <NavigationEvents onDidFocus={()=>this.loadData()} />
        <ScrollView>
        <Appbar.Header>
          <Appbar.Action
            icon="menu"
            onPress={() =>
              this.props.navigation.dispatch(DrawerActions.toggleDrawer())
            }
          />
          <Appbar.Content title="Cart" />
          <Appbar.Action onPress={()=>this.props.navigation.navigate("Cart")} icon="cart" />
        </Appbar.Header>
        <SafeAreaView>
        <ScrollView style={styles.scrollContainer}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
          {this.renderItems()}
        </ScrollView>
          {this.renderButtons()}
        </SafeAreaView>
        </ScrollView>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fafafa"
  },
  cardContainer:{
    flex:1,
    flexDirection:"row",
    justifyContent:"space-between",
    alignItems:"baseline",
    padding:20
  },
  totalContainer:{
    flex:1,
    flexDirection:"row",
    justifyContent:"space-between",
    padding:20
  },
  scrollContainer:{
  },
  orderButton:{
  },
  spaceevenly:{
    marginTop:12,
    marginBottom:12
}
});

//make this component available to the app
export default CartScreen;
