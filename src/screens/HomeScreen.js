//import liraries
import React, { Component } from "react";
import { View, StyleSheet, FlatList, ScrollView,Image,Button,AsyncStorage } from "react-native";
import { DrawerActions } from "react-navigation-drawer";
import { Text, Appbar, Title, Avatar, Card, TextInput } from "react-native-paper";
import Carousel from "react-native-snap-carousel";
import Modal from 'react-native-modal';
import Toast from 'react-native-simple-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import {NavigationEvents} from "react-navigation";
import { TouchableHighlight } from "react-native-gesture-handler";

// create a component
class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      place: null,
      isModalVisible:false,
      pUnit:"",
      selectedQty:0.00,
      pcost:0.00,
      userId:null,
      productId:null,
      categories:[],
      spinner:false
    };
  }

  componentDidMount(){
    this.loadData();
  }

  loadData(){
    this.renderProducts("");
    this.renderCategories();
  }

  toggleModal = () => {
    this.setState({isModalVisible: !this.state.isModalVisible});
  };

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  renderModal = (Id,Unit,Cost) =>{
    this.toggleModal();
    this.setState({selectedQty:''})
    this.setState({productId:Id});
    this.setState({punit:Unit});
    this.setState({pcost:Cost});
  }

  renderCategories(){
    this.setState({spinner:true});
    fetch('http://fawaskallayi-001-site1.etempurl.com/Inventory/GetCategories', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
        })
      }).then((response) => response.json())
          .then((responseJson) => {
            this.setState({spinner:false});
            var data=JSON.parse(responseJson);
            this.setState({categories:data})
          })
          .catch((error) => {
            this.setState({spinner:false});
            alert(error.message);
          });
  }

  async addToCart() {
    this.setState({spinner:true});
    if(this.state.selectedQty===0 || this.state.selectedQty===''){
      this.setState({spinner:false});
      Toast.showWithGravity('Select Quantity.',Toast.LONG,Toast.BOTTOM);
      return;
    }
    await AsyncStorage.getItem('@id').then(value =>
      //AsyncStorage returns a promise so adding a callback to get the value
      this.setState({ userId: value })
      //Setting the value in Text
    );
    const cost=this.state.selectedQty*this.state.pcost;
    fetch('http://fawaskallayi-001-site1.etempurl.com/Cart/AddtoCart', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          UserId:parseInt(this.state.userId),
          ProductId:parseInt(this.state.productId),
          Qty:parseFloat(this.state.selectedQty),
          Cost:parseFloat(cost)
        })
      }).then((response) => response.json())
          .then((responseJson) => {
            this.setState({spinner:false});
            var data=JSON.parse(responseJson);
            if(data){
              Toast.showWithGravity('Added to Cart.',Toast.LONG,Toast.BOTTOM);
              this.toggleModal();
            }
          })
          .catch((error) => {
            this.setState({spinner:false});
            this.toggleModal();
            alert(error.message);
          });
  }

  renderProductByCategory(categoryId){
    this.setState({spinner:true});
    if(categoryId==0){
      this.renderProducts("");
    }
    else{
    fetch('http://fawaskallayi-001-site1.etempurl.com/Inventory/GetAllProductsByCategory', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CategoryId:categoryId
      })
    }).then((response) => response.json())
        .then((responseJson) => {
          this.setState({spinner:false});
          var data=JSON.parse(responseJson);
          this.setState({place:data})
        })
        .catch((error) => {
          this.setState({spinner:false});
          //alert(error.message);
        });
      }
  }

Noproducts(){
  var obj=[];
  if(this.state.place!=null && this.state.place.length==0){
    obj.push(
      <>
      <Card style={{
                  elevation:1,
                  borderColor:"#000",
                  margin:10,
                }}>
                    <Card.Title title="No Products Found" />
                </Card>
      </>
    );
  }
  return obj;
}

  renderProducts(name){
    this.setState({spinner:true});
    fetch('http://fawaskallayi-001-site1.etempurl.com/Inventory/GetAllProducts', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          Name:name
        })
      }).then((response) => response.json())
          .then((responseJson) => {
            this.setState({spinner:false});
            var data=JSON.parse(responseJson);
            this.setState({place:data})
          })
          .catch((error) => {
            this.setState({spinner:false});
            alert(error.message);
          });
  }

  render() {
    return (
      <View>
        <NavigationEvents onDidFocus={()=>this.loadData()} />
        <Appbar.Header>
          <Appbar.Action
            icon="menu"
            onPress={() =>
              this.props.navigation.dispatch(DrawerActions.toggleDrawer())
            }
          />
          <Appbar.Content title="Home" />
          <Appbar.Action onPress={()=>this.props.navigation.navigate("Cart")} icon="cart" />
        </Appbar.Header>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps={'handled'}
          style={{ marginLeft: 10, marginRight: 10 }}
        >
          <Title>Popular Categories</Title>
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={this.state.categories}
            keyExtractor={(item, index) => item.label}
            renderItem={({ item: rowData }) => {
              return (
                <TouchableHighlight onPress={()=>this.renderProductByCategory(rowData.Id)}>
                <View style={styles.popularCategories}>
                  <Avatar.Image size={100} source={{ uri: `data:image/png;base64,${rowData.Image}` }} />
                  <Text style={styles.popularText}
                  >{rowData.Name}</Text>
                </View>
                </TouchableHighlight>
              );
            }}
          />
          <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
          />
          <Modal isVisible={this.state.isModalVisible}>
          <View style={{backgroundColor:'white'}}>
          <Appbar.Action style={{}} icon="close"
          onPress={()=> this.toggleModal()} />
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{marginLeft:10}}>Qty</Text>
              <TextInput style={{width:270,marginLeft:10}}
              value={this.state.selectedQty}
              onChangeText={(val) => this.updateInputVal(val, 'selectedQty')}
              keyboardType={'numeric'} />
              <Text style={{paddingLeft:10,fontWeight:'bold'}}>{this.state.punit}</Text>
            </View>
            <Text></Text>
            <Button title="Add to Cart" onPress={ () => this.addToCart()} />
          </View>
        </Modal>
        <TextInput
                    style={styles.spaceevenly}
                    mode="outlined"
                    label="Search Product"
                    keyboardType="default"
                    onChangeText={(val) => this.renderProducts(val)}
        />
          <Title style={{marginTop:15}}>Products</Title>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.place}
            keyExtractor={(item, index) => item.Id}
            renderItem={({ item: rowData }) => {
              if(rowData!=null){
              var pId=rowData.Id;
              var pname=rowData.Name;
              var pcost=rowData.CostPerUnit;
              var punit=rowData.UnitShortName.trim();
              const items=pname.trim()+" ("+rowData.LocalName.trim()+") ₹"+rowData.CostPerUnit+"/"+rowData.UnitShortName.trim();
              return (
                <Card style={{
                  elevation:1,
                  borderColor:"#000",
                  margin:10,
                }}
                 onPress={() => this.renderModal(pId,punit,pcost)}>
                    <Card.Title title={items} />
                    <Card.Cover source={{ uri: `data:image/png;base64,${rowData.ImageString}` }} />
                </Card>
              );
              }
            }}
          />
          {this.Noproducts()}
        </ScrollView>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    padding: 20
  },
  popularCategories: {
    flex: 1,
    alignItems: "center",
    margin: 7
  },
  popularText: { marginTop: 5, fontSize: 17 }
});

//make this component available to the app
export default HomeScreen;
