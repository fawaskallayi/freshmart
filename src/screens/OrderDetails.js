//import liraries
import React, { Component } from "react";
import { View,FlatList,StyleSheet,ScrollView,SafeAreaView} from "react-native";
import {Appbar,Card,Text,Button} from "react-native-paper";
import {DrawerActions} from "react-navigation-drawer";
import Spinner from 'react-native-loading-spinner-overlay';
import {NavigationEvents} from "react-navigation";

// create a component
class OrderScreen extends Component {
  constructor(props){
    super(props);
    this.state ={
      orderData:null,
      productData:null,
      ordernumber:0,
      spinner:false
    }
  }

  componentDidMount(){
   this.loadData();
  }

  async loadData(){
   await this.setState({ordernumber:this.props.navigation.state.params.orderNumber});
    this.renderOrders();
  }

  trackOrder(){
    this.props.navigation.navigate(
      'TrackOrder',
      { orderNumber:this.state.ordernumber },
    );
  }

  renderOrders(){
    this.setState({spinner:true});
    fetch('http://fawaskallayi-001-site1.etempurl.com/Order/GetOrderDetails', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      OrderNumber:parseInt(this.state.ordernumber)
    })
  }).then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner:false});
        var data=JSON.parse(responseJson);
        this.setState({orderData:data.order});
        this.setState({productData:data.products});
      })
      .catch((error) => {
        this.setState({spinner:false});
        alert(error.message);
      });
  }

  renderOrder(){
    var obj=[];
    if(this.state.orderData!=null){
      obj.push(
        <>
        <Card style={{
                  elevation:1,
                  borderColor:"#000",
                  margin:10,
                  flex:1
                }}>
                    <Card.Title />
                    <View style={styles.cardContainer}>
                    <Card.Content style={styles.cardContent}>
                      <Text style={{paddingBottom:10}}>Total : ₹{this.state.orderData.TotalAmount}</Text>
                      <Text style={{paddingBottom:10}}>Date : {this.state.orderData.OrderDateFormatted}</Text>
                    </Card.Content>
                    </View>
                </Card>
        </>
      );
    }
    return obj;
  }

  renderItems = () => {
    var obj=[];var i=0;
    if(this.state.productData!=null){
      this.state.productData.forEach(element => {
        i++;
        obj.push(
          <>
          <Card>
            <Card.Content style={styles.cardContainer}>
              <Text>{element.Qty} {element.UnitShortName}</Text>
              <Text>{element.Name}</Text>
              <Text>{element.CostPerUnit}/{element.UnitShortName}</Text>
              <Text>₹{element.Total}</Text>
            </Card.Content>
          </Card>
          </>
        );
      });
    }
    return obj;
  }

  render() {
    return (
      <View>
        <NavigationEvents onDidFocus={()=>this.loadData()} />
        <Appbar.Header>
          <Appbar.Action
            icon="menu"
            onPress={() =>
              this.props.navigation.dispatch(DrawerActions.toggleDrawer())
            }
          />
          <Appbar.Content title="Order Details" />
          <Appbar.Action onPress={()=>this.props.navigation.navigate("Cart")} icon="cart" />
        </Appbar.Header>
        <ScrollView>
        <SafeAreaView>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        {this.renderItems()}
        {this.renderOrder()}
        <Text></Text>
        <Button style={styles.orderButton}
        onPress={()=>this.trackOrder()}>Track Order</Button>
        </SafeAreaView>
        </ScrollView>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  cardContainer:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-evenly"
  },
  cardContent:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-around"
  },
  totalContainer:{
    flex:1,
    flexDirection:"row",
    justifyContent:"space-between",
    padding:20
  }
});

//make this component available to the app
export default OrderScreen;
