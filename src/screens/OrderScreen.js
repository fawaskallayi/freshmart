//import liraries
import React, { Component } from "react";
import { View,FlatList,StyleSheet,ScrollView,SafeAreaView,AsyncStorage} from "react-native";
import {Appbar,Card,Text,Button} from "react-native-paper";
import {DrawerActions} from "react-navigation-drawer";
import Spinner from 'react-native-loading-spinner-overlay';
import {NavigationEvents} from "react-navigation";

// create a component
class OrderScreen extends Component {
  constructor(props){
    super(props);
    this.state ={
      orders:null,
      userId:0,
      spinner:false
    }
  }

  componentDidMount(){
      this.loadData();
  }

  async loadData(){
    await AsyncStorage.getItem('@id').then(value =>
      //AsyncStorage returns a promise so adding a callback to get the value
      this.setState({ userId: value })
      //Setting the value in Text
    );
    this.renderOrders();
  }

  detailPage(num){
    this.props.navigation.navigate(
      'OrderDetail',
      { orderNumber:num },
    );
  }

 renderNoItem(){
   var obj=[];
   if(this.state.orders==null){
    obj.push(
      <>
      <Text style={{fontWeight:'bold',textAlign:'center'}}>No Orders</Text>
      </>
    );
    return obj;
  }
 }

  renderOrders(){
    if(this.state.userId != 0){
    this.setState({spinner:true});
    fetch('http://fawaskallayi-001-site1.etempurl.com/Order/GetAllOrdersByUser', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      UserId:this.state.userId,
    })
  }).then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner:false});
        var data=JSON.parse(responseJson);
        this.setState({orders:data});
      })
      .catch((error) => {
        this.setState({spinner:false});
        alert(error.message);
      });
    }
  }

  render() {
    return (
      <View>
        <NavigationEvents onDidFocus={()=>this.loadData()} />
        <Appbar.Header>
          <Appbar.Action
            icon="menu"
            onPress={() =>
              this.props.navigation.dispatch(DrawerActions.toggleDrawer())
            }
          />
          <Appbar.Content title="Orders" />
          <Appbar.Action onPress={()=>this.props.navigation.navigate("Cart")} icon="cart" />
        </Appbar.Header>
        <ScrollView>
        <SafeAreaView>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <FlatList
        contentContainerStyle={{paddingBottom:50}}
        showsVerticalScrollIndicator={false}
        data={this.state.orders}
        renderItem={({ item: rowData }) =>{
          var num="#"+rowData.OrderNumber+"   Status: "+rowData.CurrentStatus ;
          return (
                <Card style={{
                  elevation:1,
                  borderColor:"#000",
                  margin:10,
                  flex:1
                }}>
                    <Card.Title title={num} />
                    <View style={styles.cardContainer}>
                    <Card.Content style={styles.cardContent}>
                      <Text>Total : ₹{rowData.TotalAmount}</Text>
                      <Text>Date : {rowData.OrderDateFormatted}</Text>
                    </Card.Content>
                    <Card.Actions>
                        <Button mode="outlined" onPress={()=>this.detailPage(rowData.OrderNumber)}>Detail</Button>
                    </Card.Actions>
                    </View>
                </Card>
              );
        }}
        />
        {this.renderNoItem()}
        </SafeAreaView>
        </ScrollView>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  cardContainer:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-evenly"
  },
  cardContent:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-around"
  }
});

//make this component available to the app
export default OrderScreen;
