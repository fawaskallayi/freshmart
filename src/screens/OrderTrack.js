//import liraries
import React, { Component } from "react";
import { View,FlatList,StyleSheet,ScrollView,SafeAreaView} from "react-native";
import {Appbar,Card,Text,Button} from "react-native-paper";
import {DrawerActions} from "react-navigation-drawer";
import Spinner from 'react-native-loading-spinner-overlay';
import {NavigationEvents} from "react-navigation";

// create a component
class OrderScreen extends Component {
  constructor(props){
    super(props);
    this.state ={
      ordernumber:0,
      statusData:null,
      spinner:false
    }
  }

  componentDidMount(){
   this.loadData();
  }

  async loadData(){
    await this.setState({ordernumber:this.props.navigation.state.params.orderNumber});
    this.renderOrders();
  }

  renderOrders(){
    this.setState({spinner:true});
    fetch('http://fawaskallayi-001-site1.etempurl.com/Order/GetAllOrderStatuses', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      OrderNumber:parseInt(this.state.ordernumber)
    })
  }).then((response) => response.json())
      .then((responseJson) => {
        this.setState({spinner:false});
        var data=JSON.parse(responseJson);
        this.setState({statusData:data});
      })
      .catch((error) => {
        this.setState({spinner:false});
        alert(error.message);
      });
  }

  renderItems = () => {
    var obj=[];var i=0;
    if(this.state.statusData!=null){
      this.state.statusData.forEach(element => {
        i++;
        obj.push(
          <>
          <Card>
            <Card.Content style={styles.cardContainer}>
              <Text>{element.Status}</Text>
            </Card.Content>
          </Card>
          </>
        );
      });
    }
    return obj;
  }

  render() {
    return (
      <View>
        <NavigationEvents onDidFocus={()=>this.loadData()} />
        <Appbar.Header>
          <Appbar.Action
            icon="menu"
            onPress={() =>
              this.props.navigation.dispatch(DrawerActions.toggleDrawer())
            }
          />
          <Appbar.Content title="Order Tracking" />
          <Appbar.Action onPress={()=>this.props.navigation.navigate("Cart")} icon="cart" />
        </Appbar.Header>
        <ScrollView>
        <SafeAreaView>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        {this.renderItems()}
        </SafeAreaView>
        </ScrollView>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff"
  },
  cardContainer:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-evenly"
  },
  cardContent:{
    flex:1,
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-around"
  },
  totalContainer:{
    flex:1,
    flexDirection:"row",
    justifyContent:"space-between",
    padding:20
  }
});

//make this component available to the app
export default OrderScreen;
