//import liraries
import React, { Component } from 'react';
import { ScrollView,View, StyleSheet,AsyncStorage,Alert} from 'react-native';
import {TextInput,Title,Button} from 'react-native-paper';
import Toast from 'react-native-simple-toast';
// create a component
class SignupScreen extends Component {

    constructor(prop){
        super(prop);
        this.state={
            firstname:"",
            lastname:"",
            email:"",
            phone:"",
            password:"",
            address:""
        }
    }

    updateInputVal = (val, prop) => {
        const state = this.state;
        state[prop] = val;
        this.setState(state);
    }

    registerUser = () => {
        if(this.state.email === '' || this.state.password === '' || this.state.firstname === '' || this.state.lastname === '' || this.state.phone === '' || this.state.address==='') {
          Alert.alert('Enter details to signup!')
        } else {
          this.setState({
            isLoading: true,
          })
          fetch('http://fawaskallayi-001-site1.etempurl.com/Register/CustomerRegister', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              FirstName: this.state.firstname,
              LastName: this.state.lastname,
              Email: this.state.email,
              Mobile: this.state.phone,
              Password: this.state.password,
              Address:this.state.address,
              Role:"Customer",
            })
          }).then((response) => response.json())
              .then((responseJson) => {
                this.setState({isLoading:false});
                var data=responseJson;
                if(data){
                  Toast.showWithGravity('Registered succesfully.',Toast.LONG,Toast.BOTTOM);
                  this.props.navigation.navigate('Login');
                }
              })
              .catch((error) => {
                this.setState({isLoading:false});
                alert(error.message);
                this.props.navigation.navigate('SignUp');
              });
        }
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <Title style={styles.title}>
                    Create New Account
                </Title>
                <TextInput
                    style={styles.spaceevenly}
                    mode="outlined"
                    label="First Name"
                    keyboardType="default"
                    value={this.state.firstname}
                    onChangeText={(val) => this.updateInputVal(val, 'firstname')}
                />
                <TextInput
                    style={styles.spaceevenly}
                    mode="outlined"
                    label="Last Name"
                    keyboardType="default"
                    value={this.state.lastname}
                    onChangeText={(val) => this.updateInputVal(val, 'lastname')}
                />
                <TextInput
                  style={styles.spaceevenly}
                  mode="outlined"
                  label="Email"
                  keyboardType="email-address"
                  value={this.state.email}
                  onChangeText={(val) => this.updateInputVal(val, 'email')}  
                />
                <TextInput
                    style={styles.spaceevenly}
                    mode="outlined"
                    label="Mobile Number"
                    keyboardType="phone-pad"
                    value={this.state.phone}
                    onChangeText={(val) => this.updateInputVal(val, 'phone')}
                />
                <TextInput
                    style={styles.spaceevenly}
                    mode="outlined"
                    label="Delivery Address"
                    keyboardType="default"
                    value={this.state.address}
                    multiline={true}
                    onChangeText={(val) => this.updateInputVal(val, 'address')}
                />
                <TextInput
                    style={styles.spaceevenly}
                    mode="outlined"
                    secureTextEntry={true} 
                    label="Password"
                    placeholder="Password"
                    value={this.state.password}
                    onChangeText={(val) => this.updateInputVal(val, 'password')}
                />
                <Button
                     style={styles.signupbutton}
                     mode="contained"
                     onPress={()=>this.registerUser()}>
                     Sign Up
                </Button>
            </ScrollView>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        padding:20
    },
    title:{
        alignItems:"flex-start",
        color:"#64d",
        fontWeight:"bold",
        fontSize:22
    },
    signupbutton:{
        padding:10,
        marginLeft:15,
        width:300,
        borderRadius:10,
        borderWidth:2,
        marginTop:12
    },
    spaceevenly:{
        marginTop:12,
        marginBottom:12
    }
});

//make this component available to the app
export default SignupScreen;
