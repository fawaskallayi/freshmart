//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,Image,AsyncStorage } from 'react-native';
import {Button} from 'react-native-paper';

// create a component
class WelcomeScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            name:null
        }
    }
    static navigationOptions = { header: null };

    async componentDidMount(){
        await AsyncStorage.getItem('@name').then(value =>
            //AsyncStorage returns a promise so adding a callback to get the value
            this.setState({ name: value })
            //Setting the value in Text
        );
        if(this.state.name===null || this.state.name ===''){
            return;
        }
        else{
            this.props.navigation.navigate('Home');
        }
    }

    render() {
        return (
            <View style={style.container}>
                <Image 
                source={
                    require('../../assets/icons8-tableware-100.png')
                    }
                />
                <Text 
                    style={style.headingText}>
                    Welcome to FreshMart
                </Text>
                <Text 
                    style={style.tagline}>Order Items and Deliver at Your Home!
                </Text>
                <Button 
                    onPress={()=> this.props.navigation.navigate('Login')} 
                    style={style.login} mode="contained">
                    Login
                </Button>
                <Button 
                    style={style.signup}
                    mode="outlined"
                    onPress={()=>this.props.navigation.navigate('Signup')}>
                    Signup
                </Button>
            </View>
        );
    }
}


const style = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        fontFamily:"Roboto-Regular"
    },
    headingText:{
        paddingTop:5,
        fontSize:30,
        fontWeight:"bold"
    },
    tagline:{
        paddingTop:10,
        color:"#bcbcbc",
        fontSize:20,
        textAlign:"center"
    },
    login:{
        marginTop:15,
        width:300,
        borderRadius:10,
        borderWidth:2
    },
    signup:{
        marginTop:15,
        width:300,
        borderRadius:10,
        borderWidth:2
    }


})


//make this component available to the app
export default WelcomeScreen;
